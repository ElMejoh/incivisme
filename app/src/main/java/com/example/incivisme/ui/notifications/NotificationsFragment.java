package com.example.incivisme.ui.notifications;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.incivisme.Incidencia;
import com.example.incivisme.R;
import com.example.incivisme.SharedViewModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class NotificationsFragment extends Fragment {

    // ViewModels
    private NotificationsViewModel notificationsViewModel;
    private SharedViewModel sharedViewModel;

    private View view;

    private TextView text_notifications;
    private Button btn_notificar;

    @SuppressLint("StaticFieldLeak")
    public static ProgressBar prgBar_notificar;

    @SuppressLint("StaticFieldLeak")
    public static FusedLocationProviderClient mFusedLocationClient;

    public static Boolean mTrackingLocation;

    private static TextInputEditText latitud;
    private static TextInputEditText longitud;
    private static TextInputEditText direccion;
    private static TextInputEditText descripcion;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // ViewModels
        notificationsViewModel = new ViewModelProvider(this).get(NotificationsViewModel.class);
        sharedViewModel = new ViewModelProvider(this).get(SharedViewModel.class);

        // View
        view = inflater.inflate(R.layout.fragment_notifications, container, false);

        findView();

        ViewModelSetters(notificationsViewModel);

        mTrackingLocation = false;

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());

        btn_notificar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mTrackingLocation){
                    NotificationsViewModel.startTrackingLocation();
                } else {
                    NotificationsViewModel.stopTrackingLocation();
                }
            }
        });

        return view;
    }

    private void ViewModelSetters(NotificationsViewModel notificationsViewModel) {
        // Texto Título
        notificationsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                text_notifications.setText(s);
            }
        });

        // Texto Botón
        notificationsViewModel.getBtnText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                btn_notificar.setText(s);
            }
        });

        sharedViewModel.getCurrentLatLng().observe(getViewLifecycleOwner(), new Observer<LatLng>() {
            @Override
            public void onChanged(LatLng latLng) {
                latitud.setText(String.valueOf(latLng.latitude));
                longitud.setText(String.valueOf(latLng.longitude));
            }
        });

        sharedViewModel.getCurrentAddress().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(String s) {
                direccion.setText(s);
            }
        });
    }

    private void findView() {

        // Title - TextView
        text_notifications = view.findViewById(R.id.text_notifications);

        // Button
        btn_notificar = view.findViewById(R.id.btn_notificar);

        // Progress Bar
        prgBar_notificar = view.findViewById(R.id.prgBar_notificar);

        // txtInpLay_latitud, txtInpLay_longitud, txtInpLay_direccion, txtInpLay_descripció
        latitud = view.findViewById(R.id.txtInpEd_latitud);
        longitud = view.findViewById(R.id.txtInpEd_longitud);
        direccion = view.findViewById(R.id.txtInpEd_direccion);
        descripcion = view.findViewById(R.id.txtInpEd_descripció);

    }

    public static void postFireBase(){

        Incidencia incidencia = new Incidencia();
        incidencia.setDireccio(direccion.getText().toString());
        incidencia.setLatitud(latitud.getText().toString());
        incidencia.setLongitud(longitud.getText().toString());
        incidencia.setProblema(descripcion.getText().toString());

        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance().getReference();

        DatabaseReference users = base.child("users");
        DatabaseReference uid = users.child(auth.getUid());
        DatabaseReference incidencies = uid.child("incidencies");

        DatabaseReference reference = incidencies.push();
        reference.setValue(incidencia);

    }
}