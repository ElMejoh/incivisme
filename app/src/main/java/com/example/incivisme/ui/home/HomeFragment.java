package com.example.incivisme.ui.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.incivisme.Incidencia;
import com.example.incivisme.MainActivity;
import com.example.incivisme.R;
import com.firebase.ui.database.FirebaseListAdapter;
import com.firebase.ui.database.FirebaseListOptions;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class HomeFragment extends Fragment {

    private static HomeViewModel homeViewModel;
    private View view;
    private TextView txtVw_title;
    private ListView lsVw_incidencias;


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // ViewModel
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        // views
        view = inflater.inflate(R.layout.fragment_home, container, false);

        // Encuentra todos los elemtnos del Layout
        findView();

        // Hace cambios según el ViewModel
        ViewModelSetters(homeViewModel);

        fireBaseAdapter();


        return view;
    }

    private void fireBaseAdapter() {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        DatabaseReference base = FirebaseDatabase.getInstance().getReference();

        DatabaseReference users = base.child("users");
        DatabaseReference uid = users.child(auth.getUid());
        DatabaseReference incidencies = uid.child("incidencies");

        FirebaseListOptions<Incidencia> options = new FirebaseListOptions.Builder<Incidencia>()
                .setQuery(incidencies, Incidencia.class)
                .setLayout(R.layout.lv_incidencies)
                .setLifecycleOwner(this)
                .build();

        FirebaseListAdapter<Incidencia> adapter = new FirebaseListAdapter<Incidencia>(options) {
            @Override
            protected void populateView(View v, Incidencia model, int position) {
                TextView txtDescripcio = v.findViewById(R.id.txtVw_descripcionLv);
                TextView txtAdreca = v.findViewById(R.id.txtVw_direccionLv);

                txtDescripcio.setText(model.getProblema());
                txtAdreca.setText(model.getDireccio());
            }
        };

        ListView lvIncidencies = view.findViewById(R.id.lsVw_incidencias);
        lvIncidencies.setAdapter(adapter);
    }

    private void ViewModelSetters(HomeViewModel homeViewModel) {
        // Texto Título
        homeViewModel.getTitleText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                txtVw_title.setText(s);
            }
        });
    }

    private void findView(){
        // HOME - TextView
        txtVw_title = view.findViewById(R.id.txtVw_title);

        // Incidencies - ListView
        lsVw_incidencias = view.findViewById(R.id.lsVw_incidencias);

    }




}