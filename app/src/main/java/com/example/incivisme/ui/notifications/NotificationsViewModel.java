package com.example.incivisme.ui.notifications;

import android.Manifest;
import android.content.pm.PackageManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.incivisme.MainActivity;
import com.example.incivisme.R;
import com.example.incivisme.SharedViewModel;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;

public class NotificationsViewModel extends ViewModel {

    private static MutableLiveData<String> mTitleText;
    private static MutableLiveData<String> btnText;

    public NotificationsViewModel() {
        mTitleText = new MutableLiveData<>();
        mTitleText.setValue("Avisador d'un Problema");

        btnText = new MutableLiveData<>();
        btnText.setValue("Notificar");
    }

    public LiveData<String> getText() {
        return mTitleText;
    }

    public LiveData<String> getBtnText() {
        return btnText;
    }


    public static void startTrackingLocation(){
        if (ActivityCompat.checkSelfPermission(MainActivity.getMainActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.getMainActivity(), new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, SharedViewModel.REQUEST_LOCATION_PERMISSION);
        } else {

            NotificationsFragment.mFusedLocationClient.requestLocationUpdates(
                    getLocationRequest(),
                    mLocationCallback,
                    null
            );

            NotificationsFragment.prgBar_notificar.setVisibility(ProgressBar.VISIBLE);
            NotificationsFragment.mTrackingLocation = true;
            btnText.setValue("Cancelar");

        }
    }

    public static void stopTrackingLocation(){
        if (NotificationsFragment.mTrackingLocation){
            NotificationsFragment.mFusedLocationClient.removeLocationUpdates(mLocationCallback);

            NotificationsFragment.prgBar_notificar.setVisibility(ProgressBar.INVISIBLE);
            NotificationsFragment.mTrackingLocation = false;
            btnText.setValue("Notificar");
        }
    }

    // LocationRequest is DEPRECATED
    private static LocationRequest getLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    private static LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (NotificationsFragment.mTrackingLocation) {
                new SharedViewModel.FetchAddressTask(
                        MainActivity.getMainActivity()
                ).execute(
                        locationResult.getLastLocation()
                );
            }
        }
    };

}