package com.example.incivisme;

import android.app.Application;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.incivisme.ui.notifications.NotificationsFragment;
import com.example.incivisme.ui.notifications.NotificationsViewModel;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SharedViewModel extends AndroidViewModel {

    private static Application application;


    public static final int REQUEST_LOCATION_PERMISSION = 1;

    private static final MutableLiveData<LatLng> currentLatLng = new MutableLiveData<>();
    public static MutableLiveData<String> currentAddress = new MutableLiveData<String>();

    public SharedViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
    }

    public MutableLiveData<LatLng> getCurrentLatLng() {
        return currentLatLng;
    }
    public MutableLiveData<String> getCurrentAddress(){ return currentAddress; }

    public static class FetchAddressTask extends AsyncTask<Location, Void, String> {

        private final String TAG = FetchAddressTask.class.getSimpleName();
        private Context mContext;

        public FetchAddressTask(Context applicationContext) {
            mContext = applicationContext;
        }

        @Override
        protected String doInBackground(Location... locations) {

            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());

            Location location = locations[0];

            List<Address> addresses = null;
            String resultMessage = "";

            try {
                LatLng latlng = new LatLng(location.getLatitude(), location.getLongitude());
                currentLatLng.postValue(latlng);

                addresses = geocoder.getFromLocation(
                        location.getLatitude(),
                        location.getLongitude(),
                        1);
            }
            catch (IOException ioException) {
                resultMessage = "Servei no disponible";
                Log.e(TAG, resultMessage, ioException);
            }
            catch (IllegalArgumentException illegalArgumentException) {
                resultMessage = "Coordenades no vàlides";
                Log.e(TAG, resultMessage + ". " +
                        "Latitude = " + location.getLatitude() +
                        ", Longitude = " +
                        location.getLongitude(), illegalArgumentException);
            }

            if (addresses == null || addresses.size() == 0) {
                if (resultMessage.isEmpty()) {
                    resultMessage = "No s'ha trobat cap adreça";
                    Log.e(TAG, resultMessage);
                }
            }
            else {
                Address address = addresses.get(0);
                ArrayList<String> addressParts = new ArrayList<>();

                for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                    addressParts.add(address.getAddressLine(i));
                }

                resultMessage = TextUtils.join("\n", addressParts);
            }

            return resultMessage;
        }
        @Override
        protected void onPostExecute(String address) {
            super.onPostExecute(address);

            currentAddress.postValue(address);
            currentAddress.setValue(address);
            NotificationsViewModel.stopTrackingLocation();

            NotificationsFragment.postFireBase();
        }
    }




}
