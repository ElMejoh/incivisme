package com.example.incivisme;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class IncidenciesInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private final Activity activity;

    public IncidenciesInfoWindowAdapter(Activity activity) {
        this.activity = activity;
    }

    @Nullable
    @Override
    public View getInfoContents(@NonNull Marker marker) {

        View view = activity.getLayoutInflater().inflate(R.layout.detail_info_view, null);

        Incidencia incidencia = (Incidencia) marker.getTag();

        ImageView imagen = view.findViewById(R.id.iv_problema);
        TextView descripcion = view.findViewById(R.id.tvProblema);
        TextView direccion = view.findViewById(R.id.tvDescripcio);

        descripcion.setText(incidencia.getProblema());
        direccion.setText(incidencia.getDireccio());

        return view;
    }

    @Nullable
    @Override
    public View getInfoWindow(@NonNull Marker marker) {
        return null;
    }
}
